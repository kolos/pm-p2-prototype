#!/bin/bash

if [ -d /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin ]; then
    source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -r $TDAQ_RELEASE
else
    source /sw/tdaq/setup/setup_$TDAQ_RELEASE.sh
fi

echo using TDAQ_IPC_INIT_REF=$TDAQ_IPC_INIT_REF
export PATH=$PWD/installed/x86_64-el9-gcc13-opt/bin:$PATH
export LD_LIBRARY_PATH=$PWD/installed/x86_64-el9-gcc13-opt/lib:$LD_LIBRARY_PATH