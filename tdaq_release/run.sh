#!/bin/bash

HOST=`hostname -s`
source ./setup.sh
COMMAND=`eval echo $@`
echo starting \"$COMMAND\" command on $HOST
export TDAQ_APPLICATION_NAME=$APP_ID-$HOST
$COMMAND
