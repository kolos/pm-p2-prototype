#!/bin/bash

docker build --pull --rm -t $1:$2 -t $1:latest .
docker tag $1:$2 gitlab-registry.cern.ch/kolos/pm-p2-prototype/$1
docker push gitlab-registry.cern.ch/kolos/pm-p2-prototype/$1
docker push gitlab-registry.cern.ch/kolos/pm-p2-prototype/$1:$2
